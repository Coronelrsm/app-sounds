package cat.itb.zeldasounds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import cat.itb.zeldasounds.album.AlbumFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Fragment fragment = new AlbumFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        }


    }

}
