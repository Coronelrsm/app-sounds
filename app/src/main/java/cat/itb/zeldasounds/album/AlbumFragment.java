package cat.itb.zeldasounds.album;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.zeldasounds.R;

public class AlbumFragment extends Fragment {

    @BindView(R.id.naviListen)
    ImageView naviListen;
    @BindView(R.id.adultLink)
    ImageView adultLink;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.guideline2)
    Guideline guideline2;
    @BindView(R.id.guideline3)
    Guideline guideline3;
    @BindView(R.id.guideline4)
    Guideline guideline4;
    @BindView(R.id.item)
    ImageView item;
    @BindView(R.id.ganondorf)
    ImageView ganondorf;
    @BindView(R.id.heart)
    ImageView heart;
    @BindView(R.id.rupee)
    ImageView rupee;
    @BindView(R.id.goron)
    ImageView goron;
    @BindView(R.id.naviHey)
    ImageView naviHey;
    @BindView(R.id.frog)
    ImageView frog;
    @BindView(R.id.youngLink)
    ImageView youngLink;
    private AlbumViewModel mViewModel;
    MediaPlayer mediaPlayer;


    public static AlbumFragment newInstance() {
        return new AlbumFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.album_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.oot_navi_listen1);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);



    }

    @OnClick({R.id.naviListen, R.id.adultLink, R.id.guideline, R.id.guideline2, R.id.guideline3, R.id.guideline4, R.id.item, R.id.ganondorf, R.id.heart, R.id.rupee, R.id.goron, R.id.naviHey, R.id.frog, R.id.youngLink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.naviListen:
                playSound(R.raw.oot_navi_listen1);
                break;
            case R.id.adultLink:
                playSound(R.raw.oot_adultlink_attack1);
                break;
            case R.id.item:
                playSound(R.raw.oot_fanfare_item);
                break;
            case R.id.ganondorf:
                playSound(R.raw.oot_ganondorf_laugh);
                break;
            case R.id.heart:
                playSound(R.raw.oot_get_heart);
                break;
            case R.id.rupee:
                playSound(R.raw.oot_get_rupee);
                break;
            case R.id.goron:
                playSound(R.raw.oot_goron_cry);
                break;
            case R.id.naviHey:
                playSound( R.raw.oot_navi_hey1);
                break;
            case R.id.frog:
                playSound(R.raw.oot_notes_frog_d);
                break;
            case R.id.youngLink:
                playSound(R.raw.oot_younglink_attack1);
                break;
        }
    }

    private void playSound(int music) {
        mediaPlayer.release(); //per aturar i alliberar memòria sinó se'ns ompliria la memoria en un moment
        mediaPlayer = null;
        mediaPlayer = MediaPlayer.create(getContext(), music);
        mediaPlayer.start();
    }
}
